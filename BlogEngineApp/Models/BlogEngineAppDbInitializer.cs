﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace BlogEngineApp.Models
{
    public class BlogEngineAppDbInitializer:DropCreateDatabaseIfModelChanges<BlogEngineAppContext>
    {
        protected override void Seed(BlogEngineAppContext context)
        {
            
            base.Seed(context);
        }
    }
}