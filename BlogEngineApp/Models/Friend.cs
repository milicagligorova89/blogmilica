﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace BlogEngineApp.Models
{
    public class Friend
    {
        [Key]
        public int FriendId { get; set; }
        public string Name { get; set; }
        
        public DateTime BirthDate { get; set; }

        public string Email { get; set; }
    }
}