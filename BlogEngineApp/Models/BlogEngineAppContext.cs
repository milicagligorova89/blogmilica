﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace BlogEngineApp.Models
{
    public class BlogEngineAppContext:DbContext
    {
        
        public DbSet<Blog> Blogs { get; set; }

        public DbSet<Post> Posts { get; set; }

       

        public DbSet<Friend> Friends { get; set; }

        
    }
}