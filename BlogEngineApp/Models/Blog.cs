﻿using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;
using BlogEngineApp.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BlogEngineApp
{
    public class Blog
    {
        public int BlogId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true,
               DataFormatString = "{0:dd/MMM}")]
        public DateTime DateCreated { get; set; }

        public ICollection<Post> Posts { get; set; }
    }
}
