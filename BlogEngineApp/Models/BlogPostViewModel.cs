﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlogEngineApp.Models
{
    public class BlogPostViewModel
    {
        public int BlogId { get; set; }
        public Post Post { get; set; }
        public Blog Blog { get; set; }
    }
}