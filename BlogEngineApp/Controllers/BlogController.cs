﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BlogEngineApp;
using BlogEngineApp.Models;

namespace BlogEngineApp.Controllers
{ 
    public class BlogController : Controller
    {
        private BlogEngineAppContext db = new BlogEngineAppContext();
        private const int PostPerPage = 4;


        //
        // GET: /Blog/

        public ViewResult Index(int? id)
        {
            int pageNumber =  id ?? 0;
            IEnumerable<Blog> blogs = (from blog in db.Blogs
                                      orderby blog.BlogId descending
                                      select blog ).Skip(pageNumber * PostPerPage).Take(PostPerPage + 1);

            ViewBag.IsPreviousLinkVisible = pageNumber > 0;
            ViewBag.IsNextLinkVisible = blogs.Count() > PostPerPage;
            ViewBag.PageNumber = pageNumber;

            return View(blogs.Take(PostPerPage));
        }


        public ViewResult OpenBlog(int blogId)
        {
            Blog blog = db.Blogs.Include("Posts").Where(b => b.BlogId == blogId).FirstOrDefault();

            return View("OpenBlog", blog);
        }


        /*
        public ViewResult Index()
        {
            return View(db.Blogs.ToList());
        }

        */


        //
        // GET: /Blog/Details/5

        public ViewResult Details(int id)
        {
            Blog blog = db.Blogs.Find(id);
            return View(blog);
        }

        //
        // GET: /Blog/Create



        public ActionResult Create()
        {
            if (User.Identity.IsAuthenticated)
            {
                Blog blog = new Blog();
                blog.DateCreated = DateTime.Now;
                return View(blog);
            }
            else
                return RedirectToAction("Index");
        } 

        //
        // POST: /Blog/Create

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(Blog blog)
        {
            if (ModelState.IsValid)
            {
                db.Blogs.Add(blog);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            return View(blog);
        }
        
        //
        // GET: /Blog/Edit/5
 
        public ActionResult Edit(int id)
        {
            if (User.Identity.IsAuthenticated)
            {
                Blog blog = db.Blogs.Include("Posts").Where(b => b.BlogId == id).FirstOrDefault();
                return View(blog);
            }

            return RedirectToAction("Index");
        }

        //
        // POST: /Blog/Edit/5

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(Blog blog)
        {
            if (ModelState.IsValid)
            {
                db.Entry(blog).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(blog);
        }

        //
        // GET: /Blog/Delete/5
 
        public ActionResult Delete(int id)
        {
            Blog blog = db.Blogs.Find(id);
            return View(blog);
        }

        //
        // POST: /Blog/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateInput(false)]
        public ActionResult DeleteConfirmed(int id)
        {            
            Blog blog = db.Blogs.Find(id);
            db.Blogs.Remove(blog);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ViewResult Galery()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}