﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BlogEngineApp.Models;

namespace BlogEngineApp.Controllers
{ 
    public class PostController : Controller
    {
        private BlogEngineAppContext db = new BlogEngineAppContext();

        //
        // GET: /Post/

        public ViewResult Index()
        {
            return View(db.Posts.ToList());
        }

        //
        // GET: /Post/Details/5

        public ViewResult Details(int id)
        {
            Post post = db.Posts.Find(id);
            return View(post);
        }

        //
        // GET: /Post/Create
        [HttpGet]
        public ActionResult Create(int blogId)
        {
            BlogPostViewModel model = new BlogPostViewModel();
            model.BlogId = blogId;
            model.Post = new Post();
            model.Post.DateCreated = DateTime.Now;
            return View(model);
        } 

        //
        // POST: /Post/Create

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(BlogPostViewModel model, HttpPostedFileBase image)
        {
            if (ModelState.IsValid)
            {
                Blog blog = db.Blogs.Include("Posts").Where(b => b.BlogId == model.BlogId).FirstOrDefault();
                if (image != null)
                {
                    model.Post.ImageMimeType = image.ContentType;
                    model.Post.ImageData = new byte[image.ContentLength];
                    image.InputStream.Read(model.Post.ImageData, 0, image.ContentLength);
                }

                blog.Posts.Add(model.Post);
                db.Entry(blog).State = EntityState.Modified;
                db.SaveChanges();
            }

            return RedirectToAction("OpenBlog", "Blog", new { blogId = model.BlogId});
        }


        public FileContentResult GetImage(int postId)
        {
            Post post = db.Posts.FirstOrDefault(p => p.PostId == postId);
            if (post != null)
            {
                return File(post.ImageData, post.ImageMimeType);
            }
            else
            {
                return null;
            }
        }

        public ActionResult GetPostsForBlog(int blogId)
        {
            IList<Post> posts = db.Posts.Include("Blog").Where(p => p.Blog.BlogId == blogId).ToList();

            return View(posts);
        }
        
        //
        // GET: /Post/Edit/5
 
        public ActionResult Edit(int id)
        {
            Post post = db.Posts.Find(id);
            return View(post);
        }

        //
        // POST: /Post/Edit/5

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(Post post)
        {
            if (ModelState.IsValid)
            {
                db.Entry(post).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(post);
        }

        //
        // GET: /Post/Delete/5
 
        public ActionResult Delete(int id)
        {
            Post post = db.Posts.Find(id);
            return View(post);
        }

        //
        // POST: /Post/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {

            Post post = db.Posts.Find(id);
            db.Posts.Remove(post);
            db.SaveChanges();
            return RedirectToAction("OpenBlog", "Blog", new { blogId = post.BlogId });
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}