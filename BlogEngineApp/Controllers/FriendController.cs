﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BlogEngineApp.Models;

namespace BlogEngineApp.Controllers
{ 
    public class FriendController : Controller
    {
        private BlogEngineAppContext db = new BlogEngineAppContext();

        //
        // GET: /Friend/

        public ViewResult Index()
        {
            return View(db.Friends.ToList());
        }

        [HttpGet]
        public ViewResult Birthday()
        {
            if (User.Identity.IsAuthenticated)
            {
                IList<Friend> friends = db.Friends.Where(f => f.BirthDate.Day == DateTime.Now.Day && f.BirthDate.Month == DateTime.Now.Month).ToList();

                return View(friends);
            }
            else
                return View("Create");
        }


        //
        // GET: /Friend/Details/5

        public ViewResult Details(int id)
        {
            Friend friend = db.Friends.Find(id);
            return View(friend);
        }

        //
        // GET: /Friend/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /Friend/Create

        [HttpPost]
        public ActionResult Create(Friend friend)
        {
            if (ModelState.IsValid)
            {


                db.Friends.Add(friend);
                db.SaveChanges();
                if (User.Identity.IsAuthenticated)
                {
                    return RedirectToAction("Index");
                }
                else
                    return RedirectToAction("Index", "Blog");
            }

            return View(friend);
        }


       
        
        //
        // GET: /Friend/Edit/5
 
        public ActionResult Edit(int id)
        {
            Friend friend = db.Friends.Find(id);
            return View(friend);
        }

        //
        // POST: /Friend/Edit/5

        [HttpPost]
        public ActionResult Edit(Friend friend)
        {
            if (ModelState.IsValid)
            {
                db.Entry(friend).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(friend);
        }

        //
        // GET: /Friend/Delete/5
 
        public ActionResult Delete(int id)
        {
            Friend friend = db.Friends.Find(id);
            return View(friend);
        }

        //
        // POST: /Friend/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            Friend friend = db.Friends.Find(id);
            db.Friends.Remove(friend);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}